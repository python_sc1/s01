name_str = "Dave Supan"
age_num = 27
occ_str = "application specialist"
movie_str = "Marvel"
rating = 90

num1, num2, num3 = 1, 2, 3

print(f"I am {name_str}, and I am {age_num} years old, I work as a {occ_str}, and my rating for {movie_str} is {rating}")

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)