# [Section] Comments
# COmments in Python are done using "ctrl+/" or # symbol

#[Section] Python Syntax
# Hello World in Python
print("Hello World")

# [Section] Indentation
# Wherein other programming languages the indentation in code is for readability only, the indentation in Python is very important

# Indentation is used to indicate a block of code

# Similar to JS, there is no need to end statement with semicolons

# Variables are container of data. In Python, it is declared by stating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention

age = 35
middle_initial = "C"

name1, name2, name3, name4 = "John", "Paul", "George", "ringo"

print(name3)

# 1. Strings(str) - for alphanumeric and symbols

full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers (int, float, complex) - for integers, decimal and complex numbers

num_of_days = 365 #This is an integer
pi_approx = 3.1416 #This is a float
complex_num = 1 + 5j #This is a complex number, letter j represents the imaginary

print(type(secret_code))

# 3. Boolean(bool) - for truth values
# 
isLearning = True
isDifficult = False